<?php get_header(); ?>
<h1>Proyectos</h1>
<?php if ( have_posts() ) : ?>
Total: <?php echo wp_count_posts('proyecto')->publish; ?>
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Estado</th>
      <th scope="col">Título</th>
      <th scope="col">Asesor</th>
      <th scope="col">Categorías</th>
      <th scope="col">Alumnos</th>
      <th scope="col">Carrera</th>
      <th scope="col">Votos</th>
      <th scope="col" colspan="3"></th>
    </tr>
  </thead>
  <tbody>    

<?php while ( have_posts() ) : the_post(); ?>
        <?php $estado = get_field('estado');?> 
        <tr class="<?php echo ($estado == 'Aprobado')?'table-success':(($estado == 'Rechazado')?'table-danger':'') ?>">
            <td scope="row"><?php echo $estado; ?></td>
            <th scope="row"><?php the_title(); ?></th>
            <td><?php echo get_the_author_meta('user_firstname').' '.get_the_author_meta('user_lastname') ?></td>
            <td>
                <?php
                    $categorias = wp_get_post_terms( get_the_ID(), 'categoria');
                ?>
                <ul>
                <?php foreach($categorias as $cat): ?>
                    <li><a href="<?php echo get_term_link($cat->term_id,'categoria' ) ?>"><?php echo $cat->name; ?></a></li>
                <?php endforeach; ?>
                </ul>
            </td>
            <td>
                <?php
                    $alumnos = get_field('alumnos');
                ?>
                <ul>
                <?php foreach($alumnos as $alu): ?>
                    <li><b><?php echo get_field('matricula', $alu->ID) ?></b> - <?php echo $alu->post_title; ?></li>
                <?php endforeach; ?>
                </ul>
            </td>
            <td>
                <?php
                    $carreras = wp_get_post_terms( get_the_ID(), 'carrera');
                ?>
                <ul>
                <?php foreach($carreras as $car): ?>
                    <li><a href="<?php echo get_term_link($car->term_id,'carrera' ) ?>"><?php echo $car->name; ?></a></li>
                <?php endforeach; ?>
                </ul>
            </td>
            <td><?php echo get_post_meta( get_the_ID(), 'pld_like_count', true) ?></td>
            <td><a href="<?php echo get_edit_post_link( get_the_ID()); ?>"><i class="far fa-edit"></i> Editar</a></td>
            <td><a href="<?php echo get_permalink(); ?>"><i class="far fa-eye"></i> Ver</a></td>
        </tr>
<?php endwhile; ?>
<?php endif; ?>
    </tbody>
</table>
<?php get_footer(); ?>
