<?php get_header(); ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>
<h1>
  Título:
  <?php the_title(); ?>
</h1>
<?php
        $alumnos = get_field('alumnos');
    ?>
<h2>Alumnos:</h2>
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Matrícula</th>
      <th scope="col">Grupo</th>
      <th scope="col">Nombre</th>
      <th scope="col">Asesor academico</th>
      <th scope="col">Asesor empresarial</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($alumnos as $alu): ?>
    <tr>
      <th><?php echo get_field('matricula', $alu->ID) ?></th>
      <td>
        <?php $grupos = wp_get_post_terms( $alu->ID, 'grupo'); ?>
        <?php foreach($grupos as $gru): ?>
        <?php echo $gru->name; ?>
        <?php endforeach; ?>
      </td>
      <td><?php echo $alu->post_title; ?></td>
      <td>
        <?php $asa = get_field('asesor_academico', $alu->ID) ?>
        <?php echo $asa->user_firstname; ?>  <?php echo $asa->user_lastname; ?> <a href="#"><?php echo $asa->user_email; ?></a>
      </td>
      <td>
        <?php $ase = get_field('asesor_empresarial', $alu->ID) ?>
        <?php echo $ase->user_firstname; ?>  <?php echo $ase->user_lastname; ?> <a href="#"><?php echo $ase->user_email; ?></a>
      </td>
      <td><a href="<?php echo get_edit_post_link( $alu->ID); ?>"><i class="far fa-edit"></i> Editar</a></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<h2>Propuesta:</h2>
<div class="row">
    <div class="col-sm-6">
        <a target="_blank" href="<?php echo get_field('link') ?>"><i class="far fa-edit"></i> Editar</a>
    </div>
    <div class="col-sm-6 text-right">
        <?php  echo do_shortcode('[posts_like_dislike]');?>
    </div>
</div>

<div class="embed-responsive embed-responsive-16by9 mb-4">
    <iframe class="embed-responsive-item" src="<?php echo get_field('link') ?>"></iframe>
</div>
<?php comments_template( '', true ); ?>
<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>
