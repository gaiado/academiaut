<?php get_header(); ?>
<h1>Alumnos</h1>
<?php

global $wp_query;

if(isset($_GET['asesor_academico']))
{
  $args = array_merge( $wp_query->query_vars, array( 'meta_query' => [
    [
      'key' => 'asesor_academico',
      'compare' => '=',
      'value' => $_GET['asesor_academico']
    ]
  ]));
  query_posts( $args );
}


?>
<?php if ( have_posts() ) : ?>
Total: <?php echo wp_count_posts('alumno')->publish; ?>
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Matrícula</th>
      <th scope="col">Grupo</th>
      <th scope="col"><a href="?orderby=title&order=asc">Nombre</a></th>
      <th scope="col">Asesor academico</th>
      <th scope="col">Asesor empresarial</th>
      <th scope="col">Proyecto</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
  <?php while ( have_posts() ) : the_post(); ?>
    <tr>
      <th><?php echo get_field('matricula', get_the_ID()) ?></th>
      <td>
        <?php $grupos = wp_get_post_terms( get_the_ID(), 'grupo'); ?>
        <?php foreach($grupos as $gru): ?>
        <?php echo $gru->name; ?>
        <?php endforeach; ?>
      </td>
      <td><?php the_title(); ?></td>
      <td>
        <?php $asa = get_field('asesor_academico', get_the_ID()) ?>
        <?php echo $asa->user_firstname; ?>  <?php echo $asa->user_lastname; ?> <a href="?asesor_academico=<?php echo $asa->ID; ?>"><?php echo $asa->user_email; ?></a>
      </td>
      <td>
        <?php $ase = get_field('asesor_empresarial', get_the_ID()) ?>
        <?php echo $ase->user_firstname; ?>  <?php echo $ase->user_lastname; ?> <a href="#"><?php echo $ase->user_email; ?></a>
      </td>
            <?php
                $proyectos = get_posts(array(
                    'post_type' => 'proyecto',
                    'meta_query' => array(
                        array(
                            'key' => 'alumnos', // name of custom field
                            'value' => '"' . get_the_ID() . '"', 
                            'compare' => 'LIKE'
                        )
                    )
                ));
            ?>
        <?php if(count($proyectos)>0){
           foreach($proyectos as $pro): ?>      
        <?php $estado = get_field('estado',$pro->ID);?> 
        <td class="<?php echo ($estado == 'Aprobado')?'table-success':(($estado == 'Rechazado')?'table-danger':'') ?>">
            <a href="<?php echo get_permalink($pro->ID); ?>"><?php echo $pro->post_title; break; ?></a>
        </td>
        <?php endforeach; 
        }else{?>
          <td>Sin proyecto</td>
        <?php } ?>  
      <td><a href="<?php echo get_edit_post_link( get_the_ID()); ?>"><i class="far fa-edit"></i> Editar</a></td>
    </tr>
    <?php endwhile; ?>
  </tbody>
</table>
<?php endif; ?>
    </tbody>
</table>
<?php get_footer(); ?>
