<?php
require_once get_template_directory() .
  '/libs/class-wp-bootstrap-navwalker.php';

function brotherhoodcustom_post()
{
  register_post_type('proyecto', [
    'labels' => [
      'name' => __('Proyectos'),
      'singular_name' => __('Proyecto'),
    ],
    'public' => true,
    'has_archive' => true,
    'show_in_rest' => true,
    'rewrite' => ['slug' => 'proyectos'],
    'supports' => ['title','author','comments'],
    'menu_icon' => 'dashicons-nametag',
  ]);

  register_post_type('alumno', [
    'labels' => [
      'name' => __('Alumnos'),
      'singular_name' => __('Alumno'),
    ],
    'public' => true,
    'has_archive' => true,
    'show_in_rest' => true,
    'rewrite' => ['slug' => 'alumnos'],
    'supports' => ['title', 'thumbnail','author'],
    'menu_icon' => 'dashicons-id-alt',
  ]);

  register_taxonomy(
    'categoria',
    ['proyecto'],
    [
      'hierarchical' => true,
      'labels' => [
        'name' => __('Categorías'),
        'singular_name' => __('Categoría'),
      ],
      'show_in_rest' => true,
      'show_ui' => true,
    ]
  );
  register_taxonomy(
    'carrera',
    ['proyecto'],
    [
      'hierarchical' => true,
      'labels' => [
        'name' => __('Carreras'),
        'singular_name' => __('Carrera'),
      ],
      'show_in_rest' => true,
      'show_ui' => true,
    ]
  );
  register_taxonomy(
    'grupo',
    ['alumno'],
    [
      'hierarchical' => true,
      'labels' => [
        'name' => __('Grupos'),
        'singular_name' => __('Grupos'),
      ],
      'show_in_rest' => true,
      'show_ui' => true,
    ]
  );
}

function my_acf_json_save_point( $path ) 
{
  return get_stylesheet_directory() . '/acf-json';
}

function brotherhoodsetup()
{
  add_theme_support('post-thumbnails');
  //add_image_size('member-thumbnail', 360, 360, true);
}

function brotherhoodstyles()
{
  wp_enqueue_style(
    'style',
    get_template_directory_uri() . '/dist/bundle.css',
    [],     
    '0.0.2'                 
  );  
  wp_enqueue_style(
    'font-awesome',
    '//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css'
  ); 
}

function brotherhoodmenus()
{
  register_nav_menus([
    'header-menu' => __('Header Menu', 'brotherhood')
  ]);
}

add_filter('acf/settings/save_json', 'my_acf_json_save_point');

//Hooks
add_action('after_setup_theme', 'brotherhoodsetup');
add_action('after_setup_theme', 'brotherhoodmenus');
add_action('wp_enqueue_scripts', 'brotherhoodstyles');
add_action('init', 'brotherhoodcustom_post');
