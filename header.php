<!DOCTYPE html>
<html lang="es_mx" ontouchmove>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php
    bloginfo('name');
    wp_title();
    ?></title>
    <meta name="description" content="<?php bloginfo('description'); ?>" />
    <meta name="author" content="www.zonakb.com" />
    <?php wp_head(); ?>
</head>

<body >
   <header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="<?php echo get_site_url(); ?>"><?php bloginfo('name'); ?></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <?php
            wp_nav_menu(array(
                'theme_location' => 'header-menu',
                'depth' => 2,
                'container' => false,
                'items_wrap' => '%3$s',
                'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                'walker' => new WP_Bootstrap_Navwalker()
            ));
            ?>
          </ul>
        </div>
      </nav>
   </header>
   <main class="container">
          