<?php get_header(); ?>
<h1>Reporte</h1>
<?php

// WP_Query arguments
$args = array (
	'post_type' => array( 'alumno' ),
);

// The Query
$alumnos = new WP_Query( $args );


?>
<?php if ( $alumnos->have_posts() ) : ?>
Total: <?php echo wp_count_posts('alumno')->publish; ?>
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Asesor académico</th>
      <th scope="col">Grupo</th>
      <th scope="col">Matrícula</th>
      <th scope="col">Nombres</th>
      <th scope="col">Nombre de memoria</th>
    </tr>
  </thead>
  <tbody>
  <?php while ( $alumnos->have_posts() ) : $alumnos->the_post(); ?>
    <tr>
    <td>
        <?php $asa = get_field('asesor_academico', get_the_ID()) ?>
        <?php echo $asa->user_firstname; ?>  <?php echo $asa->user_lastname; ?> 
      </td>
      <td>
        <?php $grupos = wp_get_post_terms( get_the_ID(), 'grupo'); ?>
        <?php foreach($grupos as $gru): ?>
        <?php echo $gru->name; ?>
        <?php endforeach; ?>
      </td>
      <th><?php echo get_field('matricula', get_the_ID()) ?></th>
     
      <td><?php the_title(); ?></td>
     
      
            <?php
                $proyectos = get_posts(array(
                    'post_type' => 'proyecto',
                    'meta_query' => array(
                        array(
                            'key' => 'alumnos', // name of custom field
                            'value' => '"' . get_the_ID() . '"', 
                            'compare' => 'LIKE'
                        )
                    )
                ));
            ?>
        <?php if(count($proyectos)>0){
           foreach($proyectos as $pro): ?>      
        <?php $estado = get_field('estado',$pro->ID);?> 
        <td class="<?php echo ($estado == 'Aprobado')?'table-success':(($estado == 'Rechazado')?'table-danger':'') ?>">
            <a href="<?php echo get_permalink($pro->ID); ?>"><?php echo $pro->post_title; break; ?></a>
        </td>
        <?php endforeach; 
        }else{?>
          <td>Sin proyecto</td>
        <?php } ?>  
    </tr>
    <?php endwhile; ?>
  </tbody>
</table>
<?php endif; ?>
    </tbody>
</table>
<?php get_footer(); ?>
